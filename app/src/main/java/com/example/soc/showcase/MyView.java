package com.example.soc.showcase;

import android.content.Context;
import android.widget.Button;
import android.widget.RelativeLayout;

import static com.example.soc.showcase.MainActivity.*;

/**
 * Created by soc on 11/17/2017.
 */

public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        button = new Button(context);

        button.setOnClickListener(listener);

        addView(button);

        button1 = new Button(context);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(500,500);

        layoutParams.leftMargin = 500;
        layoutParams.topMargin = 500;

        button1.setLayoutParams(layoutParams);

        addView(button1);

    }

}
