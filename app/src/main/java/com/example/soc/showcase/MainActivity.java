package com.example.soc.showcase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    public static Button button;
    public static Button button1;
    public static MyListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listener = new MyListener();

        setContentView(new MyView(this));
    }
}
